// Files to cache
const cacheName = 'v3';
const appShellFiles = [
  '/',
  '/360',
  '/images/assets/setaDireita.png',
  '/images/assets/setaEsquerda.png',

  '/css/style.css',
  '/css/animate.css',
  '/js/three.min.js',
  '/js/panolens.min.js',
  '/js/sidenav.js',
  '/js/joy.js',
  '/js/main.js',
];

const assets = [
  '/images/blueprint.png',
  '/images/joystickIcon.png',
  '/images/Sala.png',
  '/images/Cozinha.png',
  '/images/Corredor.png',
  '/images/AreaServico.png',
  '/images/Quarto1.png',
  '/images/Quarto2.png',
  '/images/Suite.png',
  '/images/Varanda.png',
  '/images/WCSocial.png',
  '/images/WCSuite.png',
  "/images/assets/HID's/HID_Sala.png",
  "/images/assets/HID's/HID_Cozinha.png",
  "/images/assets/HID's/HID_AreaServico.png",
  "/images/assets/HID's/HID_Varanda.png",
  "/images/assets/HID's/HID_WCSocial.png",
  "/images/assets/HID's/HID_WCSuite.png",
  "/images/assets/HID's/HID_Quarto01.png",
  "/images/assets/HID's/HID_Corredor.png",
  "/images/assets/HID's/HID_Quarto02.png",
  "/images/assets/HID's/HID_Suite.png",
  "/images/assets/ELE's/ELE_AreaServico.png",
  "/images/assets/ELE's/ELE_Corredor.png",
  "/images/assets/ELE's/ELE_Cozinha.png",
  "/images/assets/ELE's/ELE_Quarto01.png",
  "/images/assets/ELE's/ELE_Quarto02.png",
  "/images/assets/ELE's/ELE_Sala.png",
  "/images/assets/ELE's/ELE_Suite.png",
  "/images/assets/ELE's/ELE_Varanda.png",
  "/images/assets/ELE's/ELE_WCSocial.png",
  "/images/assets/ELE's/ELE_WCSuite.png",
  "/images/assets/Vazios/ApVazio_AreaServico.png",
  "/images/assets/Vazios/ApVazio_Corredor.png",
  "/images/assets/Vazios/ApVazio_Cozinha.png",
  "/images/assets/Vazios/ApVazio_Quarto01.png",
  "/images/assets/Vazios/ApVazio_Quarto02.png",
  "/images/assets/Vazios/ApVazio_Sala.png",
  "/images/assets/Vazios/ApVazio_Suite.png",
  "/images/assets/Vazios/ApVazio_Varanda.png",
  "/images/assets/Vazios/ApVazio_WCSocial.png",
  "/images/assets/Vazios/ApVazio_WCSuite.png",
];

const contentToCache = appShellFiles;

// Installing Service Worker
self.addEventListener('install', (e) => {
  try {
    console.log('[Service Worker] Install');
    e.waitUntil((async () => {
      let interval;
      let index = 0;
      const total = assets.length;
      const cache = await caches.open(cacheName);
      console.log('[Service Worker] Caching all: app shell and content');
      await cache.addAll(contentToCache);
      interval = setInterval(async () => {
        if (index < total) {
          index += 1;
          await cache.add(assets[index]);
        } else {
          clearInterval(interval);
        }
      }, 5000);
    })());
  } catch (e) {
    console.log('Error install service work');
  }
});

// Fetching content using Service Worker
self.addEventListener('fetch', (e) => {
  try {
    e.respondWith((async () => {
      try {
        const r = await caches.match(e.request);
        console.log(`[Service Worker] Fetching resource: ${e.request.url}`);
        console.log(e);
        if (r) return r;
        const response = await fetch(e.request);
        const cache = await caches.open(cacheName);
        console.log(`[Service Worker] Caching new resource: ${e.request.url}`);
        cache.put(e.request, response.clone());
        return response;
      } catch (e) {
        console.log('ERROR CATCH');
        console.log(e);
      }
    })());
  } catch (e) {
    console.log('Error fetch service work');
  }
});
