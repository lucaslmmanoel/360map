//Criação de variaveis (setas)
const dire = {
  'NE': {
    x: 0.06,
    y: -0.02
  },
  'N': {
    x: 0.0,
    y: -0.02
  },
  'NW': {
    x: -0.06,
    y: -0.02
  },
  'E': {
    x: 0.06,
    y: 0.0
  },
  'W': {
    x: -0.06,
    y: 0.0
  },
  'SE': {
    x: 0.06,
    y: 0.02
  },
  'S': {
    x: 0.0,
    y: 0.02
  },
  'SW': {
    x: -0.06,
    y: 0.02
  }
}
setaCozinhaAreaServico = new PANOLENS.Infospot(400, "../images/assets/setaDireita.png");
setaCorredorSuite_dpsBug = new PANOLENS.Infospot(200, "../images/assets/setaDireita.png");
setaCorredorSuite = new PANOLENS.Infospot(200, "../images/assets/setaDireita.png");
setaCozinhaVaranda = new PANOLENS.Infospot(400, "../images/assets/setaEsquerda.png");
setaSuiteWCSuite = new PANOLENS.Infospot(400, "../images/assets/setaDireita.png");
setaWCSuiteCorredor = new PANOLENS.Infospot(300, "../images/assets/setaDireita.png");
setaVarandaCozinha = new PANOLENS.Infospot(400, "../images/assets/setaDireita.png");
setaCorredorQuarto2 = new PANOLENS.Infospot(300, "../images/assets/setaDireita.png");
setaAreaServicoCozinha = new PANOLENS.Infospot(400, "../images/assets/setaEsquerda.png");
setaSuiteCorredor = new PANOLENS.Infospot(450, "../images/assets/setaEsquerda.png");
setaSalaCorredor = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaSalaVaranda_dpsBug = new PANOLENS.Infospot(400, "../images/assets/setaFrente.png");
setaSalaVaranda = new PANOLENS.Infospot(400, "../images/assets/setaFrente.png");
setaCozinhaSala = new PANOLENS.Infospot(400, "../images/assets/setaFrente.png");
setaCorredorSala = new PANOLENS.Infospot(100, "../images/assets/setaFrente.png");
setaCorredorWCSuite = new PANOLENS.Infospot(200, "../images/assets/setaFrente.png");
setaCorredorWCSocial = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaCorredorQuarto1 = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaQuarto1Corredor = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaQuarto2Corredor = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaVarandaSala = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaWCSocialCorredor = new PANOLENS.Infospot(300, "../images/assets/setaFrente.png");
setaSalaCozinha = new PANOLENS.Infospot(500, "../images/assets/setaFrente.png");
setaWCSuiteSuite = new PANOLENS.Infospot(300, "../images/assets/setaEsquerda.png");
//declaração de constantes (imagens)
const planta = "../images/assets/PlantaApto.png"
const pan = document.querySelector('.pano-image');
const sala = new PANOLENS.ImagePanorama("../images/Sala.png");
const cozinha = new PANOLENS.ImagePanorama("../images/Cozinha.png");
const corredor = new PANOLENS.ImagePanorama("../images/Corredor.png");
const areaServico = new PANOLENS.ImagePanorama("../images/AreaServico.png");
const quarto1 = new PANOLENS.ImagePanorama("../images/Quarto1.png");
const quarto2 = new PANOLENS.ImagePanorama("../images/Quarto2.png");
const suite = new PANOLENS.ImagePanorama("../images/Suite.png");
const varanda = new PANOLENS.ImagePanorama("../images/Varanda.png");
const wcsocial = new PANOLENS.ImagePanorama("../images/WCSocial.png");
const wcsuite = new PANOLENS.ImagePanorama("../images/WCSuite.png");
const HID_Sala = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Sala.png");
const HID_Cozinha = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Cozinha.png");
const HID_AServico = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_AreaServico.png");
const HID_Varanda = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Varanda.png");
const HID_WCSocial = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_WCSocial.png");
const HID_WCSuite = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_WCSuite.png");
const HID_Quarto01 = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Quarto01.png");
const HID_Corredor = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Corredor.png");
const HID_Quarto02 = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Quarto02.png");
const HID_Suite = new PANOLENS.ImagePanorama("../images/assets/HID's/HID_Suite.png");
const ELE_AreaServico = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_AreaServico.png");
const ELE_Corredor = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Corredor.png");
const ELE_Cozinha = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Cozinha.png");
const ELE_Quarto1 = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Quarto01.png");
const ELE_Quarto2 = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Quarto02.png");
const ELE_Sala = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Sala.png");
const ELE_Suite = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Suite.png");
const ELE_Varanda = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_Varanda.png");
const ELE_WCSocial = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_WCSocial.png");
const ELE_WCSuite = new PANOLENS.ImagePanorama("../images/assets/ELE's/ELE_WCSuite.png");
const apVazio_areaServico = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_AreaServico.png");
const apVazio_corredor = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Corredor.png");
const apVazio_cozinha = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Cozinha.png");
const apVazio_quarto1 = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Quarto01.png");
const apVazio_quarto2 = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Quarto02.png");
const apVazio_sala = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Sala.png");
const apVazio_suite = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Suite.png");
const apVazio_varanda = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_Varanda.png");
const apVazio_wcSocial = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_WCSocial.png");
const apVazio_wcSuite = new PANOLENS.ImagePanorama("../images/assets/Vazios/ApVazio_WCSuite.png");
//Declaração do viewer e animação de rotação e sua
const viewer = new PANOLENS.Viewer({
  container: pan,
  autoRotate: rotate,
  autoRotateSpeed: 0.5,
  output: 'console'
})
var HIDActive = false;
var Preview = false;
var Vazio = false;
var rotate = false;
// Função quer redireciona pra cenas, usada no minimapa 2d
const sendToScene = (scene) => {
  viewer.setPanorama(scene);

}
const initialDirection = () => {
  viewer.OrbitControls.rotateLeft(1.2);
}
viewer.camera.position.set(-10, 0, 0).normalize();

viewer.setCameraFov(65);

// Elementos da barra de altitude
const elBarraAltituteUp = document.querySelector('.barraAltituteUp');
const elBarraAltituteDown = document.querySelector('.barraAltituteDown');
let elPontoMinimapaSelecionado = document.querySelector('.container-ponto-minimapa.selected');

//selecionando os botões no documento html
var button = document.querySelector('#btn');
var button2 = document.querySelector('#btn2');
var button3 = document.querySelector('#btn3');
var button4 = document.querySelector('#btn4');
var button5 = document.querySelector('#btn5');
var selectEl = document.querySelector('#select1');
var bar = document.querySelector('#bar');
function onChangeSelect(event) {
  const { value } = event.target;
  alterarImagem(value);
}
// Function chamada quando alterar o valor do select
function alterarImagem(value) {
  if (value === 'Sala') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Sala);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Sala);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_sala)
    } else {
      viewer.setPanorama(sala);
    }
  }
  else if (value === 'Cozinha') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Cozinha);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Cozinha);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_cozinha);
    } else {
      viewer.setPanorama(cozinha);
    }
  } else if (value === 'Corredor') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Corredor)
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Corredor);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_corredor);
    } else {
      viewer.setPanorama(corredor);
    }
  } else if (value === 'AreaServico') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_AServico);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_AreaServico);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_areaServico);
    } else {
      viewer.setPanorama(areaServico)
    }
  } else if (value === 'Quarto01') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Quarto01);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Quarto1);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_quarto1);
    } else {
      viewer.setPanorama(quarto1);
    }
  } else if (value === 'Quarto02') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Quarto02)
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Quarto2);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_quarto2);
    } else {
      viewer.setPanorama(quarto2);
    }
  } else if (value === 'Suite') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Suite)
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Suite);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_suite);
    } else {
      viewer.setPanorama(suite);
    }
  } else if (value === 'Varanda') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_Varanda);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_Varanda);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_varanda);
    } else {
      viewer.setPanorama(varanda);
    }
  } else if (value === 'WCSocial') {
    if (HIDActive === true) {
      viewer.setPanorama(HID_WCSocial);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_WCSocial);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_wcSocial);
    } else {
      viewer.setPanorama(wcsocial);
    }
  } else {
    if (HIDActive === true) {
      viewer.setPanorama(HID_WCSuite);
    } else if (Preview === true) {
      viewer.setPanorama(ELE_WCSuite);
    } else if (Vazio === true) {
      viewer.setPanorama(apVazio_wcSuite);
    } else {
      viewer.setPanorama(wcsuite);
    }
  }
  elPontoMinimapaSelecionado = document.querySelector('.container-ponto-minimapa.selected');
  elPontoMinimapaSelecionado.classList.remove('selected');
  elPontoMinimapaSelecionado = document.getElementById(`c-${value}`);
  elPontoMinimapaSelecionado.classList.add('selected');
  alterarCoresBarraAltitude();
}
//Criando a função de target em panorama (Cria a mudança de uma visão para outra)
function onButtonClick() {
  button.classList.remove('btn_selected')
  button2.classList.remove('btn_selected')
  button3.classList.remove('btn_selected')
  HIDActive = !HIDActive;
  if (HIDActive === true) {
    button.classList.add('btn_selected')
    Preview = false;
    Vazio = false;
  }
  alterarImagem(selectEl.value);
}
function onButtonClick2() {
  button2.classList.remove('btn_selected')
  button.classList.remove('btn_selected')
  button3.classList.remove('btn_selected')
  Preview = !Preview;
  if (Preview === true) {
    button2.classList.add('btn_selected')
    HIDActive = false;
    Vazio = false;
  }
  alterarImagem(selectEl.value);
}
function onButtonClick3() {
  button3.classList.remove('btn_selected')
  button2.classList.remove('btn_selected')
  button.classList.remove('btn_selected')
  Vazio = !Vazio;
  if (Vazio === true) {
    button3.classList.add('btn_selected')
    HIDActive = false;
    Preview = false;
  }
  alterarImagem(selectEl.value);
}
//Adicionando a função de evento nos botões para mudança de visão
selectEl.addEventListener('change', onChangeSelect);
button.addEventListener('click', onButtonClick);
button2.addEventListener('click', onButtonClick2);
button3.addEventListener('click', onButtonClick3)
//panorama (sala)
setaSalaCorredor.position.set(-906.50, -1000.88, -3000.00);
setaSalaCorredor.addEventListener('click', () => alterarImagem('Corredor'));
sala.add(setaSalaCorredor);
setaSalaCozinha.position.set(-1006.50, -1500.88, 6000.00);
setaSalaCozinha.addEventListener('click',  () => alterarImagem('Cozinha'));
sala.add(setaSalaCozinha);
setaSalaVaranda.position.set(4500.50, -1000.88, 500.00);
setaSalaVaranda.addEventListener('click',  () => alterarImagem('Varanda'));
sala.add(setaSalaVaranda);
//panorama 2 (cozinha)
setaCozinhaAreaServico.position.set(6000.00, -1000.0, 2500.00);
setaCozinhaAreaServico.addEventListener('click', () =>  alterarImagem("AreaServico"));
cozinha.add(setaCozinhaAreaServico);
setaCozinhaVaranda.position.set(6000.00, -1000.0, 700.00);
setaCozinhaVaranda.addEventListener('click', () =>  alterarImagem('Varanda'));
cozinha.add(setaCozinhaVaranda);
setaCozinhaSala.position.set(-4000.00, -1000.0, -5000.00);
setaCozinhaSala.addEventListener('click', () =>  alterarImagem('Sala'));
cozinha.add(setaCozinhaSala);
//panorama 3 (corredor)
setaCorredorSala.position.set(-50.00, -600, 1500.00);
setaCorredorSala.addEventListener('click',() =>  alterarImagem('Sala'));
corredor.add(setaCorredorSala);
setaCorredorWCSuite.position.set(0.00, -800.0, -3000.00);
setaCorredorWCSuite.addEventListener('click',() =>  alterarImagem('WCSuite'));
corredor.add(setaCorredorWCSuite);
setaCorredorWCSocial.position.set(-3500.00, -900.0, 1000.00);
setaCorredorWCSocial.addEventListener('click', () =>  alterarImagem('WCSocial'));
corredor.add(setaCorredorWCSocial);
setaCorredorQuarto1.position.set(-3000.00, -800.0, -3000.00);
setaCorredorQuarto1.addEventListener('click',() =>  alterarImagem('Quarto01'));
corredor.add(setaCorredorQuarto1);
setaCorredorQuarto2.position.set(3500.00, -800.0, -3000.00);
setaCorredorQuarto2.addEventListener('click',() =>  alterarImagem('Quarto02'));
corredor.add(setaCorredorQuarto2);
setaCorredorSuite.position.set(500.00, -800.0, -3000.00);
setaCorredorSuite.addEventListener('click',() =>  alterarImagem('Suite'));
corredor.add(setaCorredorSuite);
//panorama 4 (AreaServiço)
setaAreaServicoCozinha.position.set(-2000.00, -2000.0, -7000.00);
setaAreaServicoCozinha.addEventListener('click',   () => alterarImagem('Cozinha'));
areaServico.add(setaAreaServicoCozinha);
//panorama 5 (Quarto01)
setaQuarto1Corredor.position.set(2500.0, -900.00, 2000.00);
setaQuarto1Corredor.addEventListener('click', () => alterarImagem('Corredor'));
quarto1.add(setaQuarto1Corredor);
//panorama 6 (Quarto02)
setaQuarto2Corredor.position.set(-3000.0, -900.00, -1000.00);
setaQuarto2Corredor.addEventListener('click', () => alterarImagem('Corredor'));
quarto2.add(setaQuarto2Corredor);
//panorama 7 (Suite)
setaSuiteCorredor.position.set(-5000.00, -1300.00, 2500.00);
setaSuiteCorredor.addEventListener('click', () => alterarImagem('Corredor'));
suite.add(setaSuiteCorredor);
setaSuiteWCSuite.position.set(-4500.00, -1200.00, 250.00);
setaSuiteWCSuite.addEventListener('click',() =>  alterarImagem('WCSuite'));
suite.add(setaSuiteWCSuite);
//panorama 8 (Varanda)
setaVarandaSala.position.set(-3000.00, -300.0, -2000.00);
setaVarandaSala.addEventListener('click', () =>  alterarImagem('Sala'));
varanda.add(setaVarandaSala);
setaVarandaCozinha.position.set(-2300.00, -350.0, 5000.00);
setaVarandaCozinha.addEventListener('click', () => alterarImagem('Cozinha'));
varanda.add(setaVarandaCozinha);
//panorama 9 (WcSocial)
setaWCSocialCorredor.position.set(3000.00, -900.0, -10.00);
setaWCSocialCorredor.addEventListener('click',() => alterarImagem('Corredor'));
wcsocial.add(setaWCSocialCorredor);
//panorama 10 (WcSuite)
setaWCSuiteCorredor.position.set(2500.00, -1400.0, 3000.00);
setaWCSuiteCorredor.addEventListener('click', () => alterarImagem('Corredor'));
wcsuite.add(setaWCSuiteCorredor);
setaWCSuiteSuite.position.set(2500.00, -1000.0, 3000.00);
setaWCSuiteSuite.addEventListener('click',() =>  alterarImagem('Suite'));
wcsuite.add(setaWCSuiteSuite);
//adicionando panoramas no no viewer
viewer.add(sala, cozinha, corredor, areaServico, quarto1, quarto2, suite, varanda, wcsocial, wcsuite, HID_Sala, HID_Cozinha, HID_Corredor, HID_Quarto02,
  HID_AServico, HID_Suite, HID_Varanda, HID_WCSocial, HID_WCSuite, HID_Quarto01, ELE_AreaServico, ELE_Corredor, ELE_Cozinha, ELE_Quarto1, ELE_Quarto2, ELE_Sala,
  ELE_Suite, ELE_Varanda, ELE_WCSocial, ELE_WCSuite, apVazio_sala, apVazio_areaServico, apVazio_corredor, apVazio_cozinha, apVazio_quarto1, apVazio_quarto2,
  apVazio_suite, apVazio_varanda, apVazio_wcSocial, apVazio_wcSuite);

// Orbit controls Zoom in Zoom Out

const zoomIn = () => {
  var currentZoom = viewer.camera.fov;
  var newZoom = currentZoom - 10;
  if (newZoom < 20) newZoom = 20;
  viewer.setCameraFov(newZoom);
}

const zoomOut = () => {
  var currentZoom = viewer.camera.fov;
  var newZoom = currentZoom + 10;
  if (newZoom > 120) newZoom = 120;
  viewer.setCameraFov(newZoom);
}

const right = () => {
  viewer.OrbitControls.rotateLeft(0.08);
  viewer.disableAutoRate()
}

const left = () => {
  viewer.OrbitControls.rotateLeft(-0.08);
  viewer.disableAutoRate()
}
const rotRight = () => {
  viewer.enableAutoRate();
  if (viewer.OrbitControls.autoRotateSpeed > 0) {
    if (viewer.OrbitControls.autoRotateSpeed < 1.5) {
      viewer.OrbitControls.autoRotateSpeed += 0.3
    }
    else {
      viewer.OrbitControls.autoRotateSpeed = 1.5
    }

  } else {
    viewer.OrbitControls.autoRotateSpeed = (-1) * viewer.OrbitControls.autoRotateSpeed;
    viewer.OrbitControls.autoRotateSpeed = 0.5
  }
}

const rotLeft = () => {
  viewer.enableAutoRate();
  if (viewer.OrbitControls.autoRotateSpeed < 0) {
    if (viewer.OrbitControls.autoRotateSpeed > -1.5) {
      viewer.OrbitControls.autoRotateSpeed -= 0.3
    }
    else {
      viewer.OrbitControls.autoRotateSpeed = -1.5
    }
  } else {
    viewer.OrbitControls.autoRotateSpeed = (-1) * viewer.OrbitControls.autoRotateSpeed
    viewer.OrbitControls.autoRotateSpeed = -0.5
  }
  alterarCoresBarraAltitude();
}

const rotDown = () => {
  viewer.OrbitControls.rotateUp(0.08);
  viewer.disableAutoRate()
}
const rotUp = () => {
  viewer.OrbitControls.rotateUp(-0.08);
  viewer.disableAutoRate()
}

viewer.OrbitControls.addEventListener('change', alterarCoresBarraAltitude);

function alterarCoresBarraAltitude() {
  const posY = viewer.camera.position.y;

  elBarraAltituteUp.style.height = `0%`;
  elBarraAltituteDown.style.height = `0%`;

  if (posY < 0) {
    elBarraAltituteUp.style.height = `${-posY * 100}%`;
  } else if (posY > 0) {
    elBarraAltituteDown.style.height = `${posY * 100}%`;
  }

  const vector = viewer.camera.getWorldDirection();
  const angle = THREE.Math.radToDeg(Math.atan2(vector.x, -vector.z));

  elPontoMinimapaSelecionado.style.transform = `rotate(${angle -90}deg)`;
}

function pontoZero() {
  viewer.camera.position.y = 0;
}

const showMap2d = () =>{
  map2d = document.getElementById('planta2d')
  map2d.classList.add('animate__backOutLeft')
}
var elFull = document.querySelector('body > div.container-panolens > div.pano-image > div:nth-child(3) > span:nth-child(2)')
elFull.style.display = 'none';
full=false;
function fullScreenR(){
  if(full===false){
    document.querySelector("body").requestFullscreen();
    full=true;
  }else if(full===true){
    document.exitFullscreen();
    full=false;
  }
}
var obj1={x:'0',y:'0',dir:'C',PosX:100,PosY:100},obj2={x:0,y:0,dir:'C',PosX:100,PosY:100};

var JoyDiv = new JoyStick('joyDiv');

setInterval(function(){
  obj1.x = JoyDiv.GetX();
  obj1.y = JoyDiv.GetY();
  obj1.dir = JoyDiv.GetDir();
  obj1.PosY = JoyDiv.GetPosY();
  obj1.PosX = JoyDiv.GetPosX();
  if(JSON.stringify(obj1)!=JSON.stringify(obj2)){
    onChangeJoyStick();
  }
}, 100);
function onChangeJoyStick(){
  if(obj1.dir==='C'){
    return;
  }
  const {x, y} = dire[obj1.dir];

  viewer.OrbitControls.rotateLeft(x);
  viewer.OrbitControls.rotateUp(y);
}

function openMenu() {
  const el = document.getElementById('fundo');
  el.classList.toggle('open');
}

button4.addEventListener('click', () => openMap());
button5.addEventListener('click', () => toggleJoystick());

function openMap() {
  const map = document.getElementById('planta2d');
  map.classList.toggle('active');
}

function toggleJoystick() {
  const joystick = document.getElementById('menu-nave');
  joystick.classList.toggle('active');
}
