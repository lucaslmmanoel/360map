var  dae, path, format, loader, pointlight, intersect, infospots;

var chairModels = [];

// Disable warning from loading dae model
console.warn = ()=>{};


const pan = document.querySelector('.pano-image');
const img = "../images/Sala.png";

const panorama = new PANOLENS.ImagePanorama(img);

const viewer = new PANOLENS.Viewer({
    container: pan,
    autoRotate: false,
    autoRotateSpeed: 1,
    output: 'console'
})

viewer.add( panorama );

loader = new THREE.ColladaLoader();
loader.load( './POUPEX_Exposição_Instalações_R01.dae', function ( collada ) {

  dae = collada.scene;

  dae.scale.multiplyScalar( 5 );
  dae.position.set( -60, -20, 370 );
  dae.rotation.set( -Math.PI/2, 0, -1.9 );

  dae.traverse( child => {

    if ( child.material instanceof THREE.Material ) {
      const map = child.material.map;
      child.material = new THREE.MeshPhysicalMaterial({map, reflectivity: 0, metalness: 0});
    }

  } );

  // Get chair model
  dae.children.map( function( object ) {
    if ( object.name === 'Chair-Desk' &&
        object.material.name === 'Textile - Slate Blue' ) {
      chairModels.push( object );
    }
  } )

  panorama.add( dae );

} );

panorama.addEventListener( 'click', function( event ){

  if ( event.intersects.length > 0 ) {

    intersect = event.intersects[ 0 ].object;

    if ( !(intersect instanceof PANOLENS.Infospot) && intersect.material ) {

      if ( !intersect.previousMaterial ) {

        intersect.previousMaterial = intersect.material.clone();
        intersect.material = new THREE.MeshNormalMaterial();

      } else {

        intersect.material = intersect.previousMaterial;
        intersect.previousMaterial.dispose();
        intersect.previousMaterial = undefined;

      }

    }

  }

} );

addPointLights();
addInfospots();

function addPointLights () {

  pointlight = new THREE.PointLight( 0xFFD6AA, 0.8 );
  pointlight.position.set( 0, 0, 0 );
  panorama.add( pointlight );

  pointlight = pointlight.clone();
  pointlight.position.set( 65, 0, -45 );
  panorama.add( pointlight );


}

function addInfospots () {

  var infospot = new PANOLENS.Infospot( 1.5, PANOLENS.DataImage.Info );
  infospot.position.set( 28.22, 5.24, 6.63 );
  infospot.addHoverText( 'Tô no teste' );

  infospot.addEventListener( "click", function(){
    this.focus();
    panorama.opacity=0;

    } );
    panorama.add( infospot );

}


function onChairColorClick ( hex ) {

  if ( chairModels ) {

    chairModels.map( function ( object ) {

      if ( object.material ) {

        object.material = new THREE.MeshPhongMaterial({ color: hex });

      }

    } );

  }

}
